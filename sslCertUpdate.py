import json
import logging
import requests
import os
from requests_toolbelt.multipart.encoder import MultipartEncoder
from authenticateSession import conn
from authenticateSession import headers
from endSession import endSession
from activate import activate

logging.basicConfig(level=logging.DEBUG, filename='zia-api.log')

def generateCsr(headers, conn, payload):
    logging.debug("generateCsr Using payload: " + str(payload))
    
    conn.request("POST", "/api/v1/sslSettings/generatecsr", json.dumps(payload), headers=headers)

    res = conn.getresponse()
    data = res.read()

    conn.close()
    logging.debug("generateCsr Response: " + data.decode("utf-8"))

def getCsr(headers, conn):
    conn.request("GET", "/api/v1/sslSettings/downloadcsr", headers=headers)
    res = conn.getresponse()
    data = res.read()

    logging.debug("getCsr Response: " + data.decode("utf-8"))
    conn.close()
    return data.decode("utf-8")

def showCert(headers, conn):
    conn.request("GET", "/api/v1/sslSettings/showcert", headers=headers)
    res = conn.getresponse()
    data = res.read()

    logging.debug("showCert Response: " + data.decode("utf-8"))
    conn.close()
    return data.decode("utf-8")

def uploadCert(headers, conn, certFile):
    file = open(certFile, 'rb')
    
    payload = MultipartEncoder(
        fields={
            'fileUpload': (os.path.basename(certFile), file, 'application/x-x509-ca-cert')
        })
    logging.debug("uploadCert Using payload: " + str(payload))

    res = requests.post('https://' + conn.host + '/api/v1/sslSettings/uploadcert/text', data=payload, headers={'Content-Type': payload.content_type, 'cookie': headers['cookie']})
    logging.debug("uploadCert Response: " + res.text)
    conn.close()