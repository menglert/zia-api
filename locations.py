import logging

logging.basicConfig(level=logging.DEBUG, filename='zia-api.log')

def getLocations(headers, conn):
    conn.request("GET", "/api/v1/locations", headers=headers)
    res = conn.getresponse()
    data = res.read()
    logging.debug("Getting Response for getLocations: " + data.decode("utf-8"))
    
    return data
