import time
import getpass
import logging

logging.basicConfig(level=logging.DEBUG, filename='zia-api.log')
 
def obfuscateApiKey (apiKey):
    now = int(time.time() * 1000)
    n = str(now)[-6:]
    r = str(int(n) >> 1).zfill(6)
    obfuscatedKey = ""
    for i in range(0, len(str(n)), 1):
        obfuscatedKey += apiKey[int(str(n)[i])]
    for j in range(0, len(str(r)), 1):
        obfuscatedKey += apiKey[int(str(r)[j])+2]

    logging.debug("Obfuscated ApiKey: " + apiKey + " at Timestamp: " + str(now) + ". Result Key is: " + obfuscatedKey)
    logging.info("Obfuscated API Key.")
    return obfuscatedKey, now