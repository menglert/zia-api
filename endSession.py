import logging

logging.basicConfig(level=logging.DEBUG, filename='zia-api.log')

def endSession(headers, conn):
    conn.request("DELETE", "/api/v1/authenticatedSession", headers=headers)
    res = conn.getresponse()
    data = res.read()
    logging.debug("Getting Response for endSession: " + data.decode("utf-8"))
    
    conn.close()