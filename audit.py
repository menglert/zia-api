# Get Audit Logs
import json
import logging
from authenticateSession import conn
from authenticateSession import headers
from endSession import endSession
import time

logging.basicConfig(level=logging.DEBUG, filename='zia-api.log')

def postAuditlogEntryReport(headers, conn):
    currentTime = int(time.time() * 1000)
    previousTime = currentTime - 1800000

    payload = {
        "startTime": previousTime,
        "endTime": currentTime,
        "page": 1,
        "pageSize": 100
    }
    logging.debug("auditlogEntryReport Payload: " + json.dumps(payload))

    conn.request("POST", "/api/v1/auditlogEntryReport", json.dumps(payload), headers=headers)

    res = conn.getresponse()
    data = res.read()

    conn.close()
    logging.debug("POST auditlogEntryReport Response: " + data.decode("utf-8"))

def getAuditlogEntryReport(headers, conn):
    conn.request("GET", "/api/v1/auditlogEntryReport", headers=headers)

    res = conn.getresponse()
    data = res.read()

    conn.close()
    logging.debug("GET auditlogEntryReport Response: " + data.decode("utf-8"))


def downloadAuditlogEntryReport(headers, conn):
    conn.request("GET", "/api/v1/auditlogEntryReport/download", headers=headers)
    res = conn.getresponse()
    data = res.read()

    logging.debug("Download auditlogEntryReport Response: " + data.decode("utf-8"))
    conn.close()
    return data.decode("utf-8")