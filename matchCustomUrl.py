import re
import logging
from urlCategory import getUrlCategories
from authenticateSession import headers
from authenticateSession import conn

logging.basicConfig(level=logging.DEBUG, filename='zia-api.log')

check = [""]
dataJson = getUrlCategories(headers, conn, "true")
for category in dataJson:
    for url in category["urls"]:
        for checkUrl in check:
            logging.info(   "Match found in Category " + category["configuredName"] + 
                            " - URL " + re.search(checkUrl, url).string + 
                            " matched for Search String " + checkUrl)