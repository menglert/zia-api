import logging

logging.basicConfig(level=logging.DEBUG, filename='zia-api.log')

def activate (headers, conn):
    conn.request("POST", "/api/v1/status/activate", headers=headers)

    res = conn.getresponse()
    data = res.read()
    logging.debug("Getting Response for activate: " + data.decode("utf-8"))