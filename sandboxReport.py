import logging

logging.basicConfig(level=logging.DEBUG, filename='zia-api.log')

sandboxEndpoint = "/api/v1/sandbox/report/"

def getSandboxReport(detail, md5, headers, conn):
    conn.request("GET", sandboxEndpoint + md5 + "?details=" + detail, headers=headers)

    res = conn.getresponse()
    data = res.read()
    logging.debug("Getting Response for getSandboxReport for File MD5 " + md5 + " : " + data.decode("utf-8"))

    conn.close()
    return data