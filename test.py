import json
import pprint
import logging
import time
import re
from authenticateSession import conn
from authenticateSession import headers
from authenticateSession import apiKey
from sandboxReport import getSandboxReport
from sandboxSample import submitSampleApi
from urlLookup import postUrlLookup
from urlCategory import getUrlCategoriesLite
from urlCategory import getUrlCategory
from urlCategory import getUrlCategories

logging.basicConfig(level=logging.DEBUG, filename='zia-api.log')

def printSandboxReport():
    pp = pprint.PrettyPrinter(indent=4)
    data = [""]

    results = open("sandbox_results.log", "w")

    for md5 in data:
        results.write("File MD5: " + md5 + " Result: " + getSandboxReport("summary", md5,headers, conn).decode("utf-8") + "\n")
        time.sleep(2)
        results.flush()
    results.close()

def submitSandboxFile():
    submitSampleApi("<key>", "<force 0 or 1>", "<file path>", "<cloud like zscloud.net>")

def urlLookup():
    data=[]
    postUrlLookup(headers, conn, data)

def urlCategories():
    getUrlCategoriesLite(headers, conn)
    getUrlCategory(headers, conn, "CUSTOM_01")
    getUrlCategories(headers, conn, "true")

#submitSandboxFile()
#printSandboxReport()
urlLookup()
#urlCategories()