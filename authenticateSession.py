import http.client
import json
import getpass
import keyring
import logging
from obfuscateKey import obfuscateApiKey

logging.basicConfig(level=logging.DEBUG, filename='zia-api.log')

try:
    configFile = open("config.json")
    logging.info("Reading configuration from: " + configFile.name)
    
    configJson = json.load(configFile)
    adminUser = configJson["adminUser"]
    zsCloud = configJson["zsCloud"]
    adminPassword = keyring.get_password("zia-api-user", adminUser)
    apiKey = keyring.get_password("zia-api-key", adminUser)
    if apiKey is None or adminPassword is None:
        raise IOError("No Credential set.")
    logging.info("Read Password for Admin User: " + adminUser + " for Cloud: " + zsCloud + " and API Key")
except IOError:
    logging.info("No config present. Creating one.")
    configJson = {}
    adminUser = input("Admin User: ")
    adminPassword = getpass.getpass(prompt="Admin Password: ")
    apiKey = getpass.getpass(prompt="API Key: ")
    zsCloud = input("Zscaler Cloud: ")
    configJson["adminUser"] = adminUser
    configJson["zsCloud"] = zsCloud
    configFile = open("config.json", "w")
    json.dump(configJson, configFile)
    logging.info("Set Password for Admin User: " + adminUser + " for Cloud: " + zsCloud + " and API Key in " + configFile.name)

    configFile.close()
    keyring.set_password("zia-api-user", adminUser, adminPassword)
    keyring.set_password("zia-api-key", adminUser, apiKey)
    logging.info("Set Password for Admin User: " + adminUser + " for Cloud: " + zsCloud + " and API Key in " + configFile.name)
finally:
    configFile.close()

apiKeyObfuscated, timeStamp = obfuscateApiKey(apiKey)
conn = http.client.HTTPSConnection("zsapi." + zsCloud)

payload = {"username": adminUser, "password": adminPassword, "apiKey": apiKeyObfuscated,"timestamp": timeStamp}

headers = {
    'content-type': "application/json",
    'cache-control': "no-cache"
}
logging.debug("Using Headers: " + str(headers))

conn.request("POST", "/api/v1/authenticatedSession", json.dumps(payload), headers)
res = conn.getresponse()
data = res.read()
logging.debug("Auth Response: " + data.decode("utf-8"))

authCookie = res.headers.get("Set-Cookie")
logging.debug("Received Set-Cookie: " + str(authCookie))

headers = {
  'content-type': "application/json",
  'cache-control': "no-cache",
  'cookie': authCookie
}
logging.debug("Using Headers: " + str(headers))
logging.info("Logged In.")