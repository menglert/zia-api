import json
import logging

logging.basicConfig(level=logging.DEBUG, filename='zia-api.log')

def postUrlLookup(headers, conn, data):
    conn.request("POST", "/api/v1/urlLookup", json.dumps(data), headers=headers)

    res = conn.getresponse()
    data = res.read()

    conn.close()
    logging.debug("GET urlLookup Response: " + json.dumps(json.loads(data.decode("utf-8")), indent=4))

    return json.loads(data.decode("utf-8"))