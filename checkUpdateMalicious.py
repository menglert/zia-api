# This Script cleans up the Malicious URLs configured within Advanced Threat Protection
#   1. Get the currently defined Malicious URLs
#   2. Chunk the URLs into Lists of {chunkSize}
#   3. Check each Sub List against the Site Review API (requires traffic to go through ZIA)
#   4. Puts all URLs which are already defined in ZURLDB as MALWARE_SITE into a new List
#   5. The previously identified sites are removed from the Custom Blacklist URLs
import json
import logging
from blacklistUrls import getBlacklistUrls
from blacklistUrls import postBlacklistUrls
from urlLookup import postUrlLookup
from authenticateSession import conn
from authenticateSession import headers
from endSession import endSession

logging.basicConfig(level=logging.DEBUG, filename='zia-api.log')

def divide_chunks(l, n):
    for i in range(0, len(l), n):
        yield l[i:i + n] 

blacklistData = getBlacklistUrls(headers, conn)
blacklistJson = json.loads(blacklistData)
removeList = []

chunkSize = 100
if blacklistJson["blacklistUrls"]:
    chunkedList = list(divide_chunks(blacklistJson["blacklistUrls"], chunkSize)) 

    for subList in chunkedList:
        checkSitesPayload = subList
        responseJson = postUrlLookup(headers, conn, checkSitesPayload)
        logging.debug("Url Lookup Result: " + json.dumps(responseJson))
      
        for url in responseJson:
            if url["urlClassificationsWithSecurityAlert"] == []: 
                logging.debug("No Security Issue found in Default Security Feeds for URL: " + url["url"]) 
            else:
                logging.debug("Security Issue found in Default Security Feeds for URL: " + url["url"] + " removing it from Custom Blacklist") 
                removeList.append(url["url"])

    postBlacklistUrls('REMOVE_FROM_LIST', removeList, headers, conn)
endSession(headers,conn)