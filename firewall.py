import json
import logging
from authenticateSession import conn
from authenticateSession import headers
from endSession import endSession
from activate import activate

logging.basicConfig(level=logging.DEBUG, filename='zia-api.log')

def postFirewallFilteringRules(headers, conn, payload):
    logging.debug("firewallFilteringRules Payload: " + json.dumps(payload))

    conn.request("POST", "/api/v1/firewallFilteringRules", json.dumps(payload), headers=headers)

    res = conn.getresponse()
    data = res.read()

    conn.close()
    logging.debug("POST firewallFilteringRules Response: " + json.dumps(json.loads(data.decode("utf-8")), indent=4))

def getFirewallFilteringRules(headers, conn):
    conn.request("GET", "/api/v1/firewallFilteringRules", headers=headers)

    res = conn.getresponse()
    data = res.read()

    conn.close()
    logging.debug("GET firewallFilteringRules Response: " + json.dumps(json.loads(data.decode("utf-8")), indent=4))

def deleteFirewallFilteringRules(headers, conn, ruleId):
    conn.request("DELETE", "/api/v1/firewallFilteringRules/" + ruleId, headers=headers)

    res = conn.getresponse()
    data = res.read()

    conn.close()
    logging.debug("DELETE firewallFilteringRules Response: " + data.decode("utf-8"))