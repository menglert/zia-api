import requests
import urllib.parse
import logging
import json

logging.basicConfig(level=logging.DEBUG, filename='zia-api.log')

def submitSampleApi(apiKey, force, file, zsCloud):
    response = requests.post("https://csbapi." + zsCloud + "/zscsb/submit?api_token=" + 
        urllib.parse.quote(apiKey) +
        "&force=" + 
        force, data=open(file, 'rb'))

    responseJson = json.loads(response.text)
    logging.debug("Getting Response for submitSampleApi: " + json.dumps(responseJson))

    response.close()
    return responseJson