import json
import logging
from activate import activate

logging.basicConfig(level=logging.DEBUG, filename='zia-api.log')

def getBlacklistUrls(headers, conn):
    conn.request("GET", "/api/v1/security/advanced", headers=headers)

    res = conn.getresponse()
    data = res.read()
    logging.debug("Getting Response for getBlacklistUrls: " + data.decode("utf-8"))
    
    return data

#Supported Actions ADD_TO_LIST, REMOVE_FROM_LIST
def postBlacklistUrls(action, urlList, headers, conn):
    payload = {
        "blacklistUrls": urlList
    }
    logging.debug("Using Payload for postBlacklistUrls: " + str(payload))

    conn.request("POST", "/api/v1/security/advanced/blacklistUrls?action=" + action, json.dumps(payload), headers)
    res = conn.getresponse()
    data = res.read()
    logging.debug("Getting Response for postBlacklistUrls: " + data.decode("utf-8") + " with HTTP Response Code: " + str(res.status) + " and HTTP Response reason: " + res.reason)
    
    activate(headers, conn)