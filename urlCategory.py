import json
import logging

logging.basicConfig(level=logging.DEBUG, filename='zia-api.log')

def getUrlCategories(headers, conn, customOnly):
    conn.request("GET", "/api/v1/urlCategories?customOnly=" + customOnly, headers=headers)

    res = conn.getresponse()
    data = res.read()
    dataJson = json.loads(data.decode("utf-8"))

    conn.close()
    logging.debug("GET urlCategories Lite Response: " + json.dumps(dataJson, indent=4))

    return dataJson

def getUrlCategoriesLite(headers, conn):
    conn.request("GET", "/api/v1/urlCategories/lite", headers=headers)

    res = conn.getresponse()
    data = res.read()

    conn.close()
    logging.debug("GET urlCategories Lite Response: " + json.dumps(json.loads(data.decode("utf-8")), indent=4))

def getUrlCategory(headers, conn, categoryId):
    conn.request("GET", "/api/v1/urlCategories/" + categoryId, headers=headers)

    res = conn.getresponse()
    data = res.read()

    conn.close()
    logging.debug("GET urlCategories Response: " + json.dumps(json.loads(data.decode("utf-8")), indent=4))